#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
#include <ctime>
#include <time.h>
#include "MQTTClient.h"
#include "ADXL345.h"
#include "I2CDevice.h"
#include <pthread.h>
#define  CPU_TEMP "/sys/class/thermal/thermal_zone0/temp"
using namespace std;
using namespace exploringRPi;

//Please replace the following address with the address of your server
#define ADDRESS    "tcp://192.168.43.116:1883"
#define CLIENTID   "rpi1"
#define AUTHMETHOD "vicky"
#define AUTHTOKEN  "anugraha"
#define TOPIC1      "ee513/CPUTemp"
#define TOPIC2      "ee513/Time"
#define TOPIC3      "ee513/Adxl"
#define QOS        1
#define TIMEOUT    10000L

float getCPUTemperature() 
{                                // get the CPU temperature
   int cpuTemp;                    // store as an int
   fstream fs;
   fs.open(CPU_TEMP, fstream::in); // read from the file
   fs >> cpuTemp;
   fs.close();
   return (((float)cpuTemp)/1000);
   }

int main(int argc, char* argv[]) 
{
   time_t rtime;
   char buff[100];
   char str_payload[1000];          // Set your max message size here
   MQTTClient client;
   MQTTClient_connectOptions opts = MQTTClient_connectOptions_initializer;
   MQTTClient_message pubmsg = MQTTClient_message_initializer;
   MQTTClient_deliveryToken token;
   MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
   opts.keepAliveInterval = 20;
   opts.cleansession = 1;
   opts.username = AUTHMETHOD;
   opts.password = AUTHTOKEN;
   int rc;

   if ((rc = MQTTClient_connect(client, &opts)) != MQTTCLIENT_SUCCESS) 
   {
       cout << "Failed to connect, return code " << rc << endl;
       return -1;
   }

    //To display the time 
     struct tm * tinfo;
     time (&rtime);
     tinfo = localtime(&rtime);
     strftime(buff,sizeof(buff),"%d-%m-%Y %H:%M:%S",tinfo);
                 
   
   //To send temperature 
     sprintf(str_payload, "{\"d\":{\"CPUTemp\": %f }}", getCPUTemperature());
     
     pubmsg.payload = str_payload;
     pubmsg.payloadlen = strlen(str_payload);
     pubmsg.qos = QOS;
     pubmsg.retained = 0;

     MQTTClient_publishMessage(client, TOPIC1, &pubmsg, &token);
     cout << "Waiting for up to " << (int)(TIMEOUT/1000) <<" seconds for publication of " << str_payload <<
  	     " \non topic " << TOPIC1 << " for ClientID: " << CLIENTID << endl;

   // To send Time 
     sprintf(str_payload, "{\"d\":{\"Time\":\"%s\" }}", buff);
    
     pubmsg.payload = str_payload;
     pubmsg.payloadlen = strlen(str_payload);
     pubmsg.qos = QOS;
     pubmsg.retained = 0;
     
     MQTTClient_publishMessage(client, TOPIC2, &pubmsg, &token);
     cout << "Waiting for up to " << (int)(TIMEOUT/1000) <<" seconds for publication of " << str_payload <<
    	     " \non topic " << TOPIC2 << " for ClientID: " << CLIENTID << endl;

    //Adxl readings 
    ADXL345 sensor(1,0x53);	
    sensor.readSensorState();
    float pt = sensor.getPitch();
    float rl = sensor.getRoll();    
	
    sprintf(str_payload, "{\"d\":{\"Pitch\": %f }, \"d\":{\"Roll\": %f }}", pt, rl);
    pubmsg.payload = str_payload;
    pubmsg.payloadlen = strlen(str_payload);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    MQTTClient_publishMessage(client, TOPIC3, &pubmsg, &token);
    cout << "Waiting for up to " << (int)(TIMEOUT/1000) <<" seconds for publication of " << str_payload <<
            	     " \non topic " << TOPIC3 << " for ClientID: " << CLIENTID << endl;

     rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
     cout << "Message with token " << (int)token << " delivered." << endl;
     MQTTClient_disconnect(client, 10000);
     MQTTClient_destroy(&client);
     return rc;

}


