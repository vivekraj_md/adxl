* MQTT connectivity protocol is used to establish M2M connection to subscribe and publish the sensor data (CPU Temperature).  
* ADXL345 is a low cost accelerometer which is used to measure the angular position with respect to gravitational force.
* Paho provides open source client implementation of MQTT.
* QT creator is used for GUI representation of the sensor data.  